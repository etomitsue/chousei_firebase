import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyDBWiuGRZPEmbjRQ2aSMIWal-4LQVboZLI",
  authDomain: "chousei-firebase-59eef.firebaseapp.com",
  databaseURL: "https://chousei-firebase-59eef.firebaseio.com",
  projectId: "chousei-firebase-59eef",
  storageBucket: "chousei-firebase-59eef.appspot.com",
  messagingSenderId: "407927911194",
  appId: "1:407927911194:web:8bb9b9d03ff9969451f2a3",
  measurementId: "G-E1380P73FQ"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
